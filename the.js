/* global Spectra, $, _ */

var STATIC_COMPLIMENTS = [
  "I'm just a computer, so I don't know what you do with your day to day. But you're really good at it. Keep it up.",
  "What a thrill you are.",
  "I like your hair.",
  "Where'd you get that shirt?",
  "If you were a Pokémon, I'd choose you.",
  "Hey, good-lookin'.",
  "You get an A+.",
  "I like the way you think.",
  "Hey, are you single?",
  "Just knowing you is a pleasure.",
  "What's your number?"
];

var LOOK_ADJECTIVES = "great lovely fantastic".split(" ");
var ARE_ADJECTIVES = "amazing intriguing brilliant smart thrilling fascinating inspiring friendly exciting pleasant cheerful gracious kind honest creative generous entertaining smashing cool awesome witty tremendous".split(" ");
var EMPHASIS = "very really absolutely perfectly".split(" ");

function randomPunctuation() {
  return (Math.random() < 0.6) ? "." : "!";
}

function compliment() {
  return _.sample([
    compliment.look,
    compliment.are,
    compliment.static, compliment.static // more likely
  ])();
}

compliment.look = function() {
  var result = ["You", "look"];
  if (Math.random() > 0.8)
    result.push(_.sample(EMPHASIS));
  result.push(_.sample(LOOK_ADJECTIVES));
  return result.join(" ") + randomPunctuation();
};

compliment.are = function() {
  var result;
  if (Math.random() > 0.5)
    result = ["You're"];
  else
    result = ["You are"];
  if (Math.random() > 0.8)
    result.push(_.sample(EMPHASIS));
  result.push(_.sample(ARE_ADJECTIVES));
  return result.join(" ") + randomPunctuation();
};

compliment.static = function() {
  return _.sample(STATIC_COMPLIMENTS);
};

$(function() {

  var ADVANCE_KEYS = [13, 32];

  var $body = $(document.body);
  var $output = $(document.getElementById("output"));

  function newCompliment() {
    var color = Spectra.random().saturate(50);
    $body.css({
      backgroundColor: color.hex(),
      color: color.isLight() ? "black" : "white"
    });
    $output.text(compliment());
  }
  newCompliment();

  var $button = $(document.createElement("button"));
  $button.text("Again!");
  $button.on("click", newCompliment);
  $body.append($button);

  $(window).on("keyup", function(event) {
    var desired = ($.inArray(event.keyCode, ADVANCE_KEYS)) != -1;
    if (desired)
      newCompliment();
  });

});
